﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Short_Links
{
    [Table("Users", Schema = "dbo")]
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public void RegisterUser(string name, string login, string password)
        {
            Console.Clear();

            User user = null;
            using (MyDBContext db = new())
            {
                user = db.Users.FirstOrDefault(x => x.Login == login);
                if (user == null)
                {
                    User newuser = new() { Name = name, Login = login, Password = password };
                    db.Users.AddRange(newuser);
                    db.SaveChanges();
                    Console.WriteLine($"Wellcome {name}, please, login in to the system");
                }
                else Console.WriteLine($"The user: {login} alredy exist");
            }
        }
        public bool LoginUser(string login, string password)
        {
            Console.Clear();
            User user = null;

            using (MyDBContext db = new())
            {
                user = db.Users.FirstOrDefault(x => x.Login == login && x.Password == password);
            }

            if (user != null)
            {
                Console.WriteLine($"Wellcome {user.Name}, entering program...");
                return false;
            }
            else Console.WriteLine($"Login or password is incorrected, please, try again or complete registration");
            return true;
        }
    }
}
