﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Short_Links
{
    [Table("Links", Schema = "dbo")]
    public class Link
    {
        public int Id { get; set; }
        public string Long_link { get; set; }
        public string Short_link { get; set; }
        public void GetShort(string long_link)

        {
            Console.Clear();
            using (MyDBContext db = new())
            {
                var link = db.Links.FirstOrDefault(x => x.Long_link == long_link);

                if (link == null)
                {
                    Random rnd = new();
                    int s = rnd.Next(1, 10000);

                    string short_link = $"short.lk/sl{s}";
                    Link newlink = new() { Long_link = long_link, Short_link = short_link };

                    db.Links.AddRange(newlink);
                    db.SaveChanges();

                    Console.WriteLine($"Your Short Link is here: {short_link}");
                    Console.WriteLine("Press any key to exit");
                    Console.ReadKey();
                }
                else Console.WriteLine($"Your Short Link is here: {link.Short_link}");
            }
        }

        public void GetLong(string short_link)
        {
            Console.Clear();
            using (MyDBContext db = new())
            {
                var link = db.Links.FirstOrDefault(x => x.Short_link == short_link);
                var exlink = db.Links.FirstOrDefault();

                if (link != null)
                {
                    Console.WriteLine($"Your Full Link is here: {link.Long_link}");
                    Console.WriteLine("Press any key to exit");
                    Console.ReadKey();
                }
                else Console.WriteLine("The link is not found, try again");
            }
        }
    }
}
