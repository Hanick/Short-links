﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace Short_Links
{
    class Program
    {
        static void Main(string[] args)
        {
            User user = new();
            Link link = new();
            bool nextiteration = true;

            while (nextiteration)
            {
                Console.Clear();
                Console.WriteLine("Do you have an account?" +
                    "\n1. Yes " +
                    "\n2. No, register me!");
                string firstchoise = Console.ReadLine();

                if (firstchoise == "1")
                {
                    Console.Clear();
                    Console.WriteLine("Please, enter your login:");
                    user.Login = Console.ReadLine().ToLower();

                    Console.WriteLine("Please, enter your password:");
                    user.Password = Console.ReadLine();

                    nextiteration = user.LoginUser(user.Login, user.Password);
                }

                if (firstchoise == "2")
                {
                    Console.Clear();
                    Console.WriteLine("To register, please, enter your name:");
                    user.Name = Console.ReadLine();

                    if (user.Name.Length <= 3 || user.Name.Length >= 16)
                    {
                        Console.WriteLine("Name must have 3-16 symbols");
                        Console.WriteLine("Press any key to continue");
                        Console.ReadKey();
                        continue;
                    }

                    Console.WriteLine("Please, enter your Login:");
                    user.Login = Console.ReadLine();

                    if (user.Login.Length <= 4 || user.Login.Length >= 12)
                    {
                        Console.WriteLine("Login must have 4-12 symbols");
                        Console.WriteLine("Press any key to continue");
                        Console.ReadKey();
                        continue;
                    }

                    Console.WriteLine("Please, enter your Password:");
                    user.Password = Console.ReadLine();

                    Regex passwordregex = new Regex("^[a-zA-Z][a-zA-Z0-9]*$");
                    bool matches = passwordregex.IsMatch(user.Password);

                    if (matches == false || user.Password.Length <= 8)
                    {
                        Console.WriteLine("Your password must be at least 8 characters long and contain an uppercase and lowercase letter");
                        Console.WriteLine("Press any key to continue");
                        Console.ReadKey();
                        continue;
                    }

                    user.RegisterUser(user.Name, user.Login, user.Password);
                }
            }
            Console.Clear();
            Console.WriteLine("Wellcome to Short Links!" +
                "\n1. Give me short link!" +
                "\n2. Give me full link!");
            string secondchoise = Console.ReadLine();

            if (secondchoise == "1")
            {


                Console.Clear();
                Console.WriteLine("Please, enter your URL:");
                link.Long_link = Console.ReadLine();

                link.GetShort(link.Long_link);

            }

            if (secondchoise == "2")
            {
                Console.Clear();
                Console.WriteLine("Please, enter our short URL:");
                link.Short_link = Console.ReadLine();

                link.GetLong(link.Short_link);
            }
        }
    }
}
